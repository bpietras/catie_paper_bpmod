* Recovery factors 1

  These are the recovery factors for all recovery options. The calibration
  factors for the MIRD and sphere are AC only. The uncertainties on the MIRD and
  sphere rfs have not yet been corrected to deal with the correlated
  uncertainties problem.

  These figures have the calibration factors on two lines.
