#!/bin/bash
latexdiff ./LAST_VERSION.tex ./CATIE_EJNMMI.tex > diff.tex
#latexdiff ./3Dmird_second_submission.tex ./3Dmird.tex > diff.tex
#latexdiff ./3Dmird_friday.tex ./3Dmird.tex > diff.tex
#makedoc TEW_paper
pdflatex diff.tex
bibtex diff.aux
pdflatex diff.tex
pdflatex diff.tex
date
evince diff.pdf &
