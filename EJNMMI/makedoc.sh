#!/bin/csh

if ( $#argv < 1 ) then
        echo "usage: makedoc DOC_NAME"
        exit 1
endif

pdflatex $argv[1].tex
bibtex $argv[1]
pdflatex $argv[1].tex
pdflatex $argv[1].tex
evince $argv[1].pdf &
