#!/usr/bin/env python3.5

# Take a stand and use an actually up to date version of some software.
# Program to use matplotlib to draw example graphs of dose calculation results.
# Want to plot the doses for different calibration factors as points with the
# dose calculated with Monte Carlo as a line above.

import matplotlib.pyplot as plt

# The data could be read from a file of some sort, probably a csv as the doses
# are calculated by hand in OLINDA for the most part.

# Data list (liver data used here)
# 113ml sphere, MIRD, patient, Monte Carlo
x_ticks_wrds = ['113 ml sphere', 'MIRD', 'Patient specific', 'Monte Carlo']
doses = [7.73,5.33, 5.18, 4.6]
dose_error = [7.73*0.05, 5.33*0.05, 5.18*0.05, 0.1]

d_spleen_ac_nbg = [12.5, 11, 10, 7.29]
de_spleen_ac_nbg = [1.1, 0.9, 0.9, 0.19]

d_liver_ac_nbg = [8.04, 5.54, 5.38, 4.52]
de_liver_ac_nbg = [0.65, 0.45, 0.43, 0.1]

d_kid_ac_nbg = [22.6, 20.7, 20.9, 36.631]
de_kid_ac_nbg = [1.7, 1.6, 1.6, 36.631*0.018]

d_liv2kids_ac_nbg = [0.0538, 0.0371, 0.0360, 0.050]
de_liv2kids_ac_nbg = [0.004, 0.003, 0.003, 0.001]
d_liv2lk_ac_nbg = 0.0182
de_liv2lk_ac_nbg = 0.0182*0.02
d_liv2rk_ac_nbg = 0.0849
de_liv2rk_ac_nbg = 0.0849*0.02

d_kids2liv_ac_nbg = [0.0239, 0.0219, 0.0221, 0.073]
de_kids2liv_ac_nbg = [0.0018, 0.0016, 0.0017, 0.073*0.03]
d_rk2liv_ac_nbg = 0.029
de_rk2liv_ac_nbg = 0.029*0.02
d_lk2liv_ac_nbg = 0.0067
de_lk2liv_ac_nbg = 0.0067*0.02

d_liv2spl_ac_nbg = [0.00926, 0.00663, 0.00644, 0.00498]
de_liv2spl_ac_nbg = [0.0008, 0.0005, 0.0005, 0.00498*0.027]

d_kids2spl_ac_nbg = [0.0366, 0.0336, 0.0338, 0.101]
de_kids2spl_ac_nbg = [0.0029, 0.0026, 0.0027, 0.101*0.03]
d_rk2spl_ac_nbg = 0.0327
de_rk2spl_ac_nbg = 0.0327*0.02
d_lk2spl_ac_nbg = 0.2594
de_lk2spl_ac_nbg = 0.2594*0.02


plt.figure(figsize=(5,5))
plt.xlabel('Calibration factor')
plt.ylabel('Dose (mGy/MBq)')
plt.title('Kidneys to spleen cross dose')

#plt.plot(doses[0:3], 'bo')
#plt.errorbar(range(3), doses[0:3], yerr=dose_error[0:3], fmt='bo')
#plt.errorbar(range(3), d_spleen_ac_nbg[0:3], yerr=de_spleen_ac_nbg[0:3], fmt='bo')
#plt.errorbar(range(3), d_liv2kids_ac_nbg[0:3], yerr=de_liv2kids_ac_nbg[0:3], fmt='bo')
#plt.errorbar(range(3), d_kid_ac_nbg[0:3], yerr=de_kid_ac_nbg[0:3], fmt='bo')
#plt.errorbar(range(3), d_kids2liv_ac_nbg[0:3], yerr=de_kids2liv_ac_nbg[0:3], fmt='bo')
#plt.errorbar(range(3), d_liv2spl_ac_nbg[0:3], yerr=de_liv2spl_ac_nbg[0:3], fmt='bo')
plt.errorbar(range(3), d_kids2spl_ac_nbg[0:3], yerr=de_kids2spl_ac_nbg[0:3], fmt='bo')

# Want to avoid having point markers cut by edge of image.
xticks, xticklabels = plt.xticks()
# shift half a step to the left
# x0 - (x1 - x0) / 2 = (3 * x0 - x1) / 2
xmin = (3*xticks[0] - xticks[1])/2.
# shaft half a step to the right
xmax = (3*xticks[-1] - xticks[-2])/2.
plt.xlim(xmin, xmax)
# Set tick labels to words
plt.xticks(range(3), x_ticks_wrds[0:3])

# plot Monte Carlo value as horizontal line.
plt.axhline(y=d_kids2spl_ac_nbg[3], color='b', linewidth=1)
# Plot span to show uncertainty.
# Need to reduce thickness of edge line
plt.axhspan(d_kids2spl_ac_nbg[3]-de_kids2spl_ac_nbg[3],d_kids2spl_ac_nbg[3]+de_kids2spl_ac_nbg[3], facecolor='r', alpha=0.5, edgecolor='b', linewidth=0.5)
plt.gca().annotate('Both kidneys', xy=(1,0.05), xytext=(1,0.12))

# Plot doses to individual kidneys
# plt.axhline(y=d_liv2lk_ac_nbg, color='k', linewidth=1)
# plt.axhspan(d_liv2lk_ac_nbg-de_liv2lk_ac_nbg,d_liv2lk_ac_nbg+de_liv2lk_ac_nbg, facecolor='r', alpha=0.5, edgecolor='r', linewidth=0.5)
# plt.gca().annotate('Left kidney', xy=(1,0.0182), xytext=(0,0.02))

# plt.axhline(y=d_liv2rk_ac_nbg, color='b', linewidth=1)
# plt.axhspan(d_liv2rk_ac_nbg-de_liv2rk_ac_nbg,d_liv2rk_ac_nbg+de_liv2rk_ac_nbg, facecolor='b', alpha=0.5, edgecolor='b', linewidth=0.5)
# plt.gca().annotate('Right kidney', xy=(1,0.0849), xytext=(1,0.079))

# Plot doses from individual kidneys
plt.axhline(y=d_lk2spl_ac_nbg, color='k', linewidth=1)
plt.axhspan(d_lk2spl_ac_nbg-de_lk2spl_ac_nbg,d_lk2spl_ac_nbg+de_lk2spl_ac_nbg, facecolor='r', alpha=0.5, edgecolor='r', linewidth=0.5)
plt.gca().annotate('Left kidney', xy=(1,0.0182), xytext=(0,0.28))

plt.axhline(y=d_rk2spl_ac_nbg, color='k', linewidth=1)
plt.axhspan(d_rk2spl_ac_nbg-de_rk2spl_ac_nbg,d_rk2spl_ac_nbg+de_rk2spl_ac_nbg, facecolor='r', alpha=0.5, edgecolor='r', linewidth=0.5)
plt.gca().annotate('Right kidney', xy=(1,0.0182), xytext=(0.2,0.037))





#plt.ylim(ymin=d_spleen_ac_nbg[3]-de_spleen_ac_nbg[3]-0.1)
#plt.show()
#plt.savefig('Spleen_old_recon_b_lines_red_fill.pdf')

plt.tight_layout()
plt.savefig('kids2spl_cross_ac_nbg.pdf')
