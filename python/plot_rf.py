#!/usr/bin/env python3.5

# Take a stand and use an actually up to date version of some software.
# Plot recovery factors.

import matplotlib.pyplot as plt

# The data could be read from a file of some sort, probably a csv as the doses
# are calculated by hand in OLINDA for the most part.

# Data list (liver data used here)
# 113ml sphere, MIRD, patient, Monte Carlo
x_ticks_wrds = ['113 ml sphere', 'MIRD', 'Patient specific']
rf = [1.6, 1.12, 0.97]
rf_err = [0.1, 0.04, 0.04]
plt.figure(figsize=(5,5))
plt.xlabel('Calibration factor')
plt.ylabel('Recovery factor')

#plt.plot(doses[0:3], 'bo')
plt.errorbar(range(3), rf[0:3], yerr=rf_err[0:3], fmt='bo')

# Want to avoid having point markers cut by edge of image.
xticks, xticklabels = plt.xticks()
# shift half a step to the left
# x0 - (x1 - x0) / 2 = (3 * x0 - x1) / 2
xmin = (3*xticks[0] - xticks[1])/2.
# shaft half a step to the right
xmax = (3*xticks[-1] - xticks[-2])/2.
plt.xlim(xmin, xmax)
# Set tick labels to words
plt.xticks(range(3), x_ticks_wrds[0:3])

# plot 1 as a horizontal line
plt.axhline(y=1, color='b', linewidth=1)

#plt.ylim(ymin=doses[3]-dose_error[3]-0.1)
#plt.show()
plt.savefig('Liver_rf_SC.pdf')
